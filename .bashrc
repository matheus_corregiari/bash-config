#!/bin/bash

## Only import this file when running an
# interactive session, otherwise, exit
[[ $- == *i* ]] || return

## Set the Bash Facilities directory, where all
# files to be included must be in.
export BFDIR="$HOME/.bash_facilities"

# #Import Bash Aliases
export BASH_CONF="bash_profile"
source "$BFDIR/01_aliases.sh"

## Import Bash Functions
 source "$BFDIR/02_functions.sh"

## Import Bash Environment Variables
source "$BFDIR/03_variables.sh"

## Import Configurations
source "$BFDIR/04_configs.sh"

#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#

## Bash completion, we love it :)

# if [ -f "$(brew --prefix bash-git-prompt)/share/gitprompt.sh"   ]; then
# 	GIT_PROMPT_THEME=Default
#	source "$(brew --prefix bash-git-prompt)/share/gitprompt.sh"
# fi

# if [ -f `brew --prefix`/etc/bash_completion ]; then
#     . `brew --prefix`/etc/bash_completion
# fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

source ~/.bash-git-prompt/gitprompt.sh
  GIT_PROMPT_ONLY_IN_REPO=0

if [ -f $HOME/.git-completion.bash ]; then
  . $HOME/.git-completion.bash
fi


export NVM_DIR="/home/matheus/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

eval `ssh-agent -s` &> /dev/null
