## Since I started using MAC, need to know where I am.

SYSTEM=$(uname -s)

## A better history control
export HISTCONTROL=ignoreboth
export HISTSIZE=100000
export HISTFILESIZE=200000

if test $SYSTEM == Linux
then
	## Set the terminal type
	export TERM=xterm
	## Set the input module
	export GTK_IM_MODULE=ibus
	export XMODIFIERS=@im=ibus
	export QT_IM_MODULE=ibus
	export OOO_FORCE_DESKTOP=kde
else
	export CLICOLOR=1
	export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
fi

## Java Home
export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk1.8.0_241.jdk/Contents/Home/"

## Android Home
export ANDROID_HOME=/Users/matheus.corregiari/Library/Android/sdk/

## Define the default editor
export EDITOR="vim"

## Lets guarantee a good PATH
export PATH="$PATH:/bin"
export PATH="$PATH:/sbin"
export PATH="$PATH:/usr/bin"
export PATH="$PATH:/usr/sbin"
export PATH="$PATH:/usr/local/bin"
export PATH="$PATH:/usr/local/sbin"
export PATH="$PATH:$HOME/bin"
export PATH="$PATH:$HOME/sbin"
export PATH="$PATH:$HOME/dev/flutter/bin"
## Optional items
export PATH="$PATH:/opt/metasploit-framework"

export PATH=$PATH:$ANDROID_HOME/bin:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/lib
export PATH=$PATH:$ANDROID_HOME/tools/lib:$ANDROID_HOME/bin
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$JAVA_HOME
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
export PATH="/opt/android-studio/bin:$PATH"
 
## Extra Itens
export LOAD="config/load_classes.rb"
export ANDROID_HVPROTO='ddm'

## Work Arround to make Android Emulator Work! =(
alias emulator=$ANDROID_HOME/emulator/emulator
