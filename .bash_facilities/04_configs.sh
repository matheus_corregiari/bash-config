## Automatically resize the terminal
#MACshopt -s checkwinsize

## Disable the terminal bell
#MACif [ ! -z $DISPLAY ]
#MACthen
#MAC  xset -b
#MAC  xmodmap ~/.Xmodmap
#MAC  xhost + &> /dev/null
#MACfi

## Include autocompletion for Amazon API commands
complete -C aws_completer aws

## Include rbenv

## Update Ruby-build
#cd .rbenv/plugins/ruby-build && (git pull -q || true) && cd
#cd .rbenv && (git pull -q || true) && cd

#eval "$(rbenv init -)"
#rbenv global 2.2.3
