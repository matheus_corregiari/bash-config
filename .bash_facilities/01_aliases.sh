## Here you can define your default options for commands, or even
# create your own commands using bash aliases
SYSTEM=$(uname -s)

alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias igrep='grep --color=auto -i'

if test $SYSTEM == Linux
then
	alias l='ls -alF --color=auto'
	alias l.='ls -d .* --color=auto'
	alias ll='ls -l --color=auto'
	alias ls='ls --color=auto'
	alias open='xdg-open'
fi

alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'

alias gitG="git log --graph --all --color --pretty=format:'%C(auto)%h%Creset %C(auto)%D%Creset (%Cgreen%an%Creset): %C(white)%s%Creset - %C(yellow)%cr%Creset' --abbrev-commit --date=relative"
alias gitGD="git log --graph --decorate --name-status $1 --pretty=format:'%C(auto)%H %C(auto)%d %n Autor: %Cblue%aN%Creset (%ae) %n Date:  %aD %n%n %Cgreen ------- '%s' ------- %Creset %n'"

alias gs='git status'
alias gd='git diff'
alias gaa='git add --all'
alias gcmv='git commit'
alias gcmi='git commit -m'
alias gpl='git pull --rebase'
alias gpm='git push origin master'
alias gps='git push origin'
alias gco='git checkout'
alias gcb='git checkout -b'
alias gpc='git push origin $(git branch | head -n 1 | cut -d " " -f 2-)'


alias ccat='pygmentize -O style=monokai -f console256 -g'
alias dog='ccat'


## alias vim="vim -u ~/.vimrc"

alias sonar="$HOME/.sonar/bin/macosx-universal-64/sonar.sh"
